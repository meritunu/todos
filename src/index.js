import React from 'react';
import ReactDOM from 'react-dom';
import { combineReducers } from 'redux';
import { createStore } from 'redux';

import './index.css';

import App from './components/App';

import todos from './reducers/todos';
import visibilityFilter from './reducers/visibilityFilter';

import registerServiceWorker from './registerServiceWorker';


const todoApp = combineReducers({todos, visibilityFilter});
const store = createStore(todoApp);

const render = () => {
	ReactDOM.render(
		<App store={store} />,
		document.getElementById('root'),
	);
};
store.subscribe(render);
render();

registerServiceWorker();
