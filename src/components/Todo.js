import React from 'react';
import '../styles/Todo.css';

const Todo = ({onClick, onDeleteClick, completed, text}) => (
	<li className="item">
		<button className="check" onClick={onClick}>{completed?"\u2714":"\u25EF"}</button>				
		<label className="text" style={{
			textDecoration: completed?'line-through':'none',
			color: completed?'#e3e3e3':'#6f4545'
		}}>{text}</label>
		<button className="delete" onClick={onDeleteClick}>&#10006;</button>
	</li>
);

export default Todo;