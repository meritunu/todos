import React from 'react';
import FilterLink from './FilterLink';
import '../styles/Footer.css';

const Footer = ({visibilityFilter, onFilterClick}) => {
	return (
		<div id="footer">
			<FilterLink 
				filter='SHOW_ALL'
				currentFilter={visibilityFilter}
				onClick={onFilterClick}
			>All</FilterLink>
			<FilterLink 
				filter='SHOW_ACTIVE'
				currentFilter={visibilityFilter}
				onClick={onFilterClick}
			>Active</FilterLink>
			<FilterLink
				filter='SHOW_COMPLETED'
				currentFilter={visibilityFilter}
				onClick={onFilterClick}
			>Completed</FilterLink>
		</div>
	);
};

export default Footer;