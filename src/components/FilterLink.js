import React from 'react';
import '../styles/FilterLink.css';

const FilterLink = ({filter,currentFilter,children,onClick}) => {
	if (filter === currentFilter) {
		return (<a className="btn-footer-active">{children}</a>);
	}
	return (
		<a 	className="btn-footer-deactive" 
			onClick={e => {
				e.preventDefault();
				onClick(filter);						
			}}
		>{children}</a>
	);
};

export default FilterLink;