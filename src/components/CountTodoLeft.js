import React from 'react';
import '../styles/CountTodoLeft.css';

const CountTodoLeft = ({countActiveTodo}) => {
	return (
		<div id="count-todo-left">
			{countActiveTodo} {(countActiveTodo === 1)?'item':'items'} left
		</div>
	);
};

export default CountTodoLeft;