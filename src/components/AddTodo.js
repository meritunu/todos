import React, { Component } from 'react';
import '../styles/AddTodo.css';

class AddTodo extends Component {
	constructor(...args) {
		super(...args);
		this.state = {"inputValue":"", "buttonAddClassName":"btn-add-deactive"};
	}

	render() {
		let inputValue = this.state.inputValue;
		let buttonAddClassName = this.state.buttonAddClassName;

		return (
			<div id="add-todo-wrap">
				<button id="btn-all" onClick={this.props.onAllClick}>
					<i className="arrow down" 
						style={{borderColor:this.props.isAllChecked?'#6f4545':'#e3e3e3'}}>
					</i>
				</button>
				<input id="input-add-todo" 
					placeholder="What needs to be done?" 
					value={inputValue}
					onKeyPress={e => {
						if (e.key === "Enter") {
					        if (inputValue.trim() !== "") {
								this.props.onAddClick(inputValue.trim());								
							}	
							this.setState({inputValue: "", buttonAddClassName: "btn-add-deactive"});
					    }						    
					}}
					onChange={e => {
						let newInputValue = e.target.value;
						this.setState({
							inputValue: newInputValue,
							buttonAddClassName: (newInputValue.trim() === "")?"btn-add-deactive":"btn-add-active",
						});
					}}
				/>
				<button id="btn-add" 
					className={buttonAddClassName} 
					onClick={() => {
						if (inputValue.trim() !== "") {
							this.props.onAddClick(inputValue.trim());								
						}	
						this.setState({inputValue: "", buttonAddClassName: "btn-add-deactive"});		
					}}
				>&#10010;</button>
			</div>
		);
	}
};

export default AddTodo;