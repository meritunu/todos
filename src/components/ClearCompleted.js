import React from 'react';
import '../styles/ClearCompleted.css';

const ClearCompleted = ({countCompletedTodo, onClick}) => {
	return (
		<div id="clear-completed">
			<button
				onClick={onClick}
				style={{cursor:(countCompletedTodo === 0)?'default':'pointer'}}
			>{(countCompletedTodo === 0)?'':'Clear Completed'}</button>
		</div>
	);
};

export default ClearCompleted;