import React from 'react';
import '../styles/App.css';
import AddTodo from './AddTodo';
import TodoList from './TodoList';
import Footer from './Footer';
import CountTodoLeft from './CountTodoLeft';
import ClearCompleted from './ClearCompleted';

const getVisibleTodos = (todos, filter) => {
  switch (filter) {
    case 'SHOW_ALL':
      return todos;
    case 'SHOW_COMPLETED':
      return todos.filter(t => t.completed);
    case 'SHOW_ACTIVE':
      return todos.filter(t => !t.completed);
    default:
      return todos;
  }
};
let nextTodoId = 0;
const App = ({store}) => {
  let todos = store.getState().todos;
  let visibilityFilter = store.getState().visibilityFilter;

  return (
    <div align="center">
      <h1 className="title">todos</h1>
      <div className="bg">
        <AddTodo 
          onAllClick={
            () => store.dispatch({
              type: 'TOGGLE_ALL'
            })
          }
          onAddClick={
            text => store.dispatch({
              type: 'ADD_TODO',
              text: text,
              id: nextTodoId++,
            })
          } 
          isAllChecked={((todos.filter(t => t.completed).length===todos.length) && (todos.length!==0))}
        />          
        <TodoList
          todos={getVisibleTodos(todos, visibilityFilter)}
          onTodoClick={id => store.dispatch({type: 'TOGGLE_TODO', id})}
          onTodoDeleteClick={id => store.dispatch({type: 'DELETE_TODO', id})}         
        />
        <div style={{display:(todos.length===0)?'none':'block', backgroundColor: '#fefefe'}}>
          <CountTodoLeft 
            countActiveTodo={todos.filter(t => !t.completed).length}
          />
          <Footer 
            visibilityFilter={visibilityFilter}
            onFilterClick={
              filter => store.dispatch({
                type: 'SET_VISIBILITY_FILTER',
                filter,
              })
            }
          />
          <ClearCompleted
            countCompletedTodo={todos.filter(t => t.completed).length}
            onClick={() => store.dispatch({type: 'CLEAR_COMPLETED'})}
          />
        </div>
      </div>
    </div>
  )
};

export default App;
