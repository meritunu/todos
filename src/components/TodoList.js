import React from 'react';
import Todo from './Todo';
import '../styles/TodoList.css';

const TodoList = ({todos, onTodoClick, onTodoDeleteClick}) => (
	<ul id="todo-list">
		{todos.map(todo =>
			<Todo key={todo.id}
				{...todo}
				onClick={() => onTodoClick(todo.id)} 
				onDeleteClick={() => onTodoDeleteClick(todo.id)}
			/>			
		)}
	</ul>
);

export default TodoList;