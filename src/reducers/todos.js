const todos = (state = [], action) => {
	let newState;
	switch (action.type) {
		case 'ADD_TODO':
			return [
				...state, {
					id: action.id,
					text: action.text,
					completed: false,
				},
			];
		case 'TOGGLE_TODO':
			return state.map(t => {
				if (t.id !== action.id) {
					return t;
				}

				return {
					...t, 
					completed: !t.completed,
				};
			});
		case 'TOGGLE_ALL':
			let completedCount = 0;
			for (let i = 0; i < state.length; i++) {
			    if (state[i].completed === true) {
			    	completedCount++;
				}
			}
			if (completedCount !== state.length) {
				return state.map(t => {
					return {
						...t,
						completed: true,
					};
				});
			} else {
				return state.map(t => {
					return {
						...t,
						completed: false,
					};
				});
			}
		case 'CLEAR_COMPLETED':
			newState = [];
			for (let i = 0; i < state.length; i++) {
			    if (state[i].completed === false) {
			    	newState.push(state[i]);
				}
			}
			return newState;
		case 'DELETE_TODO':
			newState = [].concat(state);
			for (let i = 0; i < newState.length; i++) {
			    if (newState[i].id === action.id) {
			    	let index = newState.indexOf(newState[i]);
			    	newState.splice(index, 1);
			    	return newState;
				}
			}
			break;
		default:
			return state;
	}
};

export default todos;